package controls;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.MessageFormat;
import java.time.Duration;

public class DropdownControl extends WebControl {

    static Logger log = LogManager.getLogger(DropdownControl.class);

    public static final String DROPDOWN = "//button[contains(., \"{0}\")]";
    public static final String LINK = "//a[contains(., \"{0}\")]";

    public DropdownControl(By locator) {
        super(locator);
        wait = new WebDriverWait(driver, Duration.ofSeconds(10));
    }

    public static DropdownControl fromLabel(String label) {
        return fromLabel(label, 1);
    }

    public static DropdownControl fromLabel(String label, int index) {
        String buttonXpath = MessageFormat.format(DROPDOWN, label);
        String linkXpath = MessageFormat.format(LINK, label);

        String xpath = "((" +
                MessageFormat.format(buttonXpath, label) +
                ") | (" +
                MessageFormat.format(linkXpath, label) +
                "))[" + index +"]";

        By locator = By.xpath(xpath);
        DropdownControl control = new DropdownControl(locator);
        control.label = label;
        return control;
    }

    public static DropdownControl fromLocator(String locator) {
        By dropdownLocator = By.xpath(locator);
        DropdownControl control = new DropdownControl(dropdownLocator);
        control.label = "";
        return control;
    }

    public void hoverOver() {
        WebElement element = driver.findElement(locator);
        Actions actions = new Actions(driver);
        actions.moveToElement(element).build().perform();
    }

    public boolean isVisible() {
        try {
            wait.until(ExpectedConditions.elementToBeClickable(this.locator));
            return true;
        } catch(Exception e) {
            return false;
        }
    }
}
